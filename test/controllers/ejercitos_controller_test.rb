require 'test_helper'

class EjercitosControllerTest < ActionController::TestCase
  setup do
    @ejercito = ejercitos(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:ejercitos)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create ejercito" do
    assert_difference('Ejercito.count') do
      post :create, ejercito: { nombre: @ejercito.nombre, puntos: @ejercito.puntos }
    end

    assert_redirected_to ejercito_path(assigns(:ejercito))
  end

  test "should show ejercito" do
    get :show, id: @ejercito
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @ejercito
    assert_response :success
  end

  test "should update ejercito" do
    patch :update, id: @ejercito, ejercito: { nombre: @ejercito.nombre, puntos: @ejercito.puntos }
    assert_redirected_to ejercito_path(assigns(:ejercito))
  end

  test "should destroy ejercito" do
    assert_difference('Ejercito.count', -1) do
      delete :destroy, id: @ejercito
    end

    assert_redirected_to ejercitos_path
  end
end
