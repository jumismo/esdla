require 'test_helper'

class ListaEjercitosControllerTest < ActionController::TestCase
  setup do
    @lista_ejercito = lista_ejercitos(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:lista_ejercitos)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create lista_ejercito" do
    assert_difference('ListaEjercito.count') do
      post :create, lista_ejercito: { nombre: @lista_ejercito.nombre, puntos: @lista_ejercito.puntos, puntos_maximos: @lista_ejercito.puntos_maximos }
    end

    assert_redirected_to lista_ejercito_path(assigns(:lista_ejercito))
  end

  test "should show lista_ejercito" do
    get :show, id: @lista_ejercito
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @lista_ejercito
    assert_response :success
  end

  test "should update lista_ejercito" do
    patch :update, id: @lista_ejercito, lista_ejercito: { nombre: @lista_ejercito.nombre, puntos: @lista_ejercito.puntos, puntos_maximos: @lista_ejercito.puntos_maximos }
    assert_redirected_to lista_ejercito_path(assigns(:lista_ejercito))
  end

  test "should destroy lista_ejercito" do
    assert_difference('ListaEjercito.count', -1) do
      delete :destroy, id: @lista_ejercito
    end

    assert_redirected_to lista_ejercitos_path
  end
end
