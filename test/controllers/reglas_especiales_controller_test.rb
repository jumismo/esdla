require 'test_helper'

class ReglasEspecialesControllerTest < ActionController::TestCase
  setup do
    @reglas_especiale = reglas_especiales(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:reglas_especiales)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create reglas_especiale" do
    assert_difference('ReglasEspeciale.count') do
      post :create, reglas_especiale: { descripcion: @reglas_especiale.descripcion, nombre: @reglas_especiale.nombre }
    end

    assert_redirected_to reglas_especiale_path(assigns(:reglas_especiale))
  end

  test "should show reglas_especiale" do
    get :show, id: @reglas_especiale
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @reglas_especiale
    assert_response :success
  end

  test "should update reglas_especiale" do
    patch :update, id: @reglas_especiale, reglas_especiale: { descripcion: @reglas_especiale.descripcion, nombre: @reglas_especiale.nombre }
    assert_redirected_to reglas_especiale_path(assigns(:reglas_especiale))
  end

  test "should destroy reglas_especiale" do
    assert_difference('ReglasEspeciale.count', -1) do
      delete :destroy, id: @reglas_especiale
    end

    assert_redirected_to reglas_especiales_path
  end
end
