require 'test_helper'

class EquipoDisponiblesControllerTest < ActionController::TestCase
  setup do
    @equipo_disponible = equipo_disponibles(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:equipo_disponibles)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create equipo_disponible" do
    assert_difference('EquipoDisponible.count') do
      post :create, equipo_disponible: { tipo_miniatura: @equipo_disponible.tipo_miniatura }
    end

    assert_redirected_to equipo_disponible_path(assigns(:equipo_disponible))
  end

  test "should show equipo_disponible" do
    get :show, id: @equipo_disponible
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @equipo_disponible
    assert_response :success
  end

  test "should update equipo_disponible" do
    patch :update, id: @equipo_disponible, equipo_disponible: { tipo_miniatura: @equipo_disponible.tipo_miniatura }
    assert_redirected_to equipo_disponible_path(assigns(:equipo_disponible))
  end

  test "should destroy equipo_disponible" do
    assert_difference('EquipoDisponible.count', -1) do
      delete :destroy, id: @equipo_disponible
    end

    assert_redirected_to equipo_disponibles_path
  end
end
