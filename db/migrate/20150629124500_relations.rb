class Relations < ActiveRecord::Migration
  def change
    create_table :tropas_equipo_disponible, id:false do |t|
      t.belongs_to :tropa, index:true
      t.belongs_to :equipo_disponible, index:true
    end
    create_table :heros_equipo_disponible, id:false do |t|
      t.belongs_to :heroe, index:true
      t.belongs_to :equipo_disponible, index:true
    end

    create_table :tropas_reglas, id:false do |t|
      t.belongs_to :tropa, index:true
      t.belongs_to :regla, index:true
    end

    create_table :heros_reglas, id:false do |t|
      t.belongs_to :heroe, index:true
      t.belongs_to :regla, index:true
    end

    create_table :tropas_lista_ejercito, id:false do |t|
      t.integer :cantidad
      t.belongs_to :tropas, index:true
      t.belongs_to :lista_ejercitos, index:true
    end

    create_table :heroes_lista_ejercito, id:false do |t|
      t.integer :cantidad
      t.belongs_to :heros, index:true
      t.belongs_to :lista_ejercitos, index:true
    end

  end
end