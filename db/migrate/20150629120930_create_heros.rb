class CreateHeros < ActiveRecord::Migration
  def change
    create_table :heros do |t|
      t.string :nombre
      t.integer :puntos
      t.integer :combate
      t.string :impacto
      t.integer :fuerza
      t.integer :defensa
      t.integer :ataque
      t.integer :herida
      t.integer :valor
      t.string :p_poder
      t.string :p_voluntad
      t.string :p_destino
      t.integer :movimiento
      t.string :descripcion

      t.timestamps null: false

      t.belongs_to :ejercito, index:true
    end
  end
end
