class CreateEquipos < ActiveRecord::Migration
  def change
    create_table :equipos do |t|
      t.string :nombre
      t.string :descripcion
      t.integer :puntos

      t.timestamps null: false
    end
  end
end
