class CreateListaEjercitos < ActiveRecord::Migration
  def change
    create_table :lista_ejercitos do |t|
      t.string :nombre
      t.integer :puntos_maximos
      t.integer :puntos

      t.timestamps null: false

      t.belongs_to :usuario, index:true
    end
  end
end
