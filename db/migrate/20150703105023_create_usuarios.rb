class CreateUsuarios < ActiveRecord::Migration
  def change
    create_table :usuarios do |t|
      t.string :usuario
      t.string :nombre
      t.string :password_hash
      t.string :password_salt


      t.timestamps null: false
    end
  end
end
