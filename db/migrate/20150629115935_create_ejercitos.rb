class CreateEjercitos < ActiveRecord::Migration
  def change
    create_table :ejercitos do |t|
      t.string :nombre
      t.string :descripcion

      t.timestamps null: false
    end
  end
end
