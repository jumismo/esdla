class CreateEquipoDisponibles < ActiveRecord::Migration
  def change
    create_table :equipo_disponibles do |t|
      t.string :tipo_miniatura

      t.timestamps null: false

      t.belongs_to :equipo, index:true
    end
  end
end
