class CreateTropas < ActiveRecord::Migration
  def change
    create_table :tropas do |t|
      t.string :nombre
      t.integer :puntos
      t.integer :combate
      t.string :impacto
      t.integer :fuerza
      t.integer :defensa
      t.integer :ataque
      t.integer :herida
      t.integer :valor
      t.integer :movimiento
      t.string :descripcion

      t.timestamps null: false

      t.belongs_to :ejercito, index:true
    end
  end
end
