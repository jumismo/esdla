class CreateReglasEspeciales < ActiveRecord::Migration
  def change
    create_table :reglas_especiales do |t|
      t.string :nombre
      t.string :descripcion

      t.timestamps null: false
    end
  end
end
