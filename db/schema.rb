# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150703105023) do

  create_table "ejercitos", force: :cascade do |t|
    t.string   "nombre",      limit: 255
    t.string   "descripcion", limit: 255
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  create_table "equipo_disponibles", force: :cascade do |t|
    t.string   "tipo_miniatura", limit: 255
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.integer  "equipo_id",      limit: 4
  end

  add_index "equipo_disponibles", ["equipo_id"], name: "index_equipo_disponibles_on_equipo_id", using: :btree

  create_table "equipos", force: :cascade do |t|
    t.string   "nombre",      limit: 255
    t.string   "descripcion", limit: 255
    t.integer  "puntos",      limit: 4
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  create_table "heroes_lista_ejercito", id: false, force: :cascade do |t|
    t.integer "cantidad",           limit: 4
    t.integer "heros_id",           limit: 4
    t.integer "lista_ejercitos_id", limit: 4
  end

  add_index "heroes_lista_ejercito", ["heros_id"], name: "index_heroes_lista_ejercito_on_heros_id", using: :btree
  add_index "heroes_lista_ejercito", ["lista_ejercitos_id"], name: "index_heroes_lista_ejercito_on_lista_ejercitos_id", using: :btree

  create_table "heros", force: :cascade do |t|
    t.string   "nombre",      limit: 255
    t.integer  "puntos",      limit: 4
    t.integer  "combate",     limit: 4
    t.string   "impacto",     limit: 255
    t.integer  "fuerza",      limit: 4
    t.integer  "defensa",     limit: 4
    t.integer  "ataque",      limit: 4
    t.integer  "herida",      limit: 4
    t.integer  "valor",       limit: 4
    t.string   "p_poder",     limit: 255
    t.string   "p_voluntad",  limit: 255
    t.string   "p_destino",   limit: 255
    t.integer  "movimiento",  limit: 4
    t.string   "descripcion", limit: 255
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.integer  "ejercito_id", limit: 4
  end

  add_index "heros", ["ejercito_id"], name: "index_heros_on_ejercito_id", using: :btree

  create_table "heros_equipo_disponible", id: false, force: :cascade do |t|
    t.integer "heroe_id",             limit: 4
    t.integer "equipo_disponible_id", limit: 4
  end

  add_index "heros_equipo_disponible", ["equipo_disponible_id"], name: "index_heros_equipo_disponible_on_equipo_disponible_id", using: :btree
  add_index "heros_equipo_disponible", ["heroe_id"], name: "index_heros_equipo_disponible_on_heroe_id", using: :btree

  create_table "heros_reglas", id: false, force: :cascade do |t|
    t.integer "heroe_id", limit: 4
    t.integer "regla_id", limit: 4
  end

  add_index "heros_reglas", ["heroe_id"], name: "index_heros_reglas_on_heroe_id", using: :btree
  add_index "heros_reglas", ["regla_id"], name: "index_heros_reglas_on_regla_id", using: :btree

  create_table "lista_ejercitos", force: :cascade do |t|
    t.string   "nombre",         limit: 255
    t.integer  "puntos_maximos", limit: 4
    t.integer  "puntos",         limit: 4
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.integer  "usuario_id",     limit: 4
  end

  add_index "lista_ejercitos", ["usuario_id"], name: "index_lista_ejercitos_on_usuario_id", using: :btree

  create_table "reglas_especiales", force: :cascade do |t|
    t.string   "nombre",      limit: 255
    t.string   "descripcion", limit: 255
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  create_table "tropas", force: :cascade do |t|
    t.string   "nombre",      limit: 255
    t.integer  "puntos",      limit: 4
    t.integer  "combate",     limit: 4
    t.string   "impacto",     limit: 255
    t.integer  "fuerza",      limit: 4
    t.integer  "defensa",     limit: 4
    t.integer  "ataque",      limit: 4
    t.integer  "herida",      limit: 4
    t.integer  "valor",       limit: 4
    t.integer  "movimiento",  limit: 4
    t.string   "descripcion", limit: 255
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.integer  "ejercito_id", limit: 4
  end

  add_index "tropas", ["ejercito_id"], name: "index_tropas_on_ejercito_id", using: :btree

  create_table "tropas_equipo_disponible", id: false, force: :cascade do |t|
    t.integer "tropa_id",             limit: 4
    t.integer "equipo_disponible_id", limit: 4
  end

  add_index "tropas_equipo_disponible", ["equipo_disponible_id"], name: "index_tropas_equipo_disponible_on_equipo_disponible_id", using: :btree
  add_index "tropas_equipo_disponible", ["tropa_id"], name: "index_tropas_equipo_disponible_on_tropa_id", using: :btree

  create_table "tropas_lista_ejercito", id: false, force: :cascade do |t|
    t.integer "cantidad",           limit: 4
    t.integer "tropas_id",          limit: 4
    t.integer "lista_ejercitos_id", limit: 4
  end

  add_index "tropas_lista_ejercito", ["lista_ejercitos_id"], name: "index_tropas_lista_ejercito_on_lista_ejercitos_id", using: :btree
  add_index "tropas_lista_ejercito", ["tropas_id"], name: "index_tropas_lista_ejercito_on_tropas_id", using: :btree

  create_table "tropas_reglas", id: false, force: :cascade do |t|
    t.integer "tropa_id", limit: 4
    t.integer "regla_id", limit: 4
  end

  add_index "tropas_reglas", ["regla_id"], name: "index_tropas_reglas_on_regla_id", using: :btree
  add_index "tropas_reglas", ["tropa_id"], name: "index_tropas_reglas_on_tropa_id", using: :btree

  create_table "usuarios", force: :cascade do |t|
    t.string   "usuario",       limit: 255
    t.string   "nombre",        limit: 255
    t.string   "password_hash", limit: 255
    t.string   "password_salt", limit: 255
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

end
