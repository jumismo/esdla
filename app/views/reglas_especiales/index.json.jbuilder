json.array!(@reglas_especiales) do |reglas_especiale|
  json.extract! reglas_especiale, :id, :nombre, :descripcion
  json.url reglas_especiale_url(reglas_especiale, format: :json)
end
