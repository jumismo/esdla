json.array!(@lista_ejercitos) do |lista_ejercito|
  json.extract! lista_ejercito, :id, :nombre, :puntos_maximos, :puntos
  json.url lista_ejercito_url(lista_ejercito, format: :json)
end
