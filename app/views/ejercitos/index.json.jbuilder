json.array!(@ejercitos) do |ejercito|
  json.extract! ejercito, :id, :nombre,
  json.url ejercito_url(ejercito, format: :json)
end
