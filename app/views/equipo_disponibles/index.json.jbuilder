json.array!(@equipo_disponibles) do |equipo_disponible|
  json.extract! equipo_disponible, :id, :tipo_miniatura
  json.url equipo_disponible_url(equipo_disponible, format: :json)
end
