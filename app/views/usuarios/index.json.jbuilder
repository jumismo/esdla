json.array!(@usuarios) do |usuario|
  json.extract! usuario, :id, :usuario, :password, :nombre
  json.url usuario_url(usuario, format: :json)
end
