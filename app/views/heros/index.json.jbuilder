json.array!(@heros) do |hero|
  json.extract! hero, :id, :nombre, :puntos, :combate, :impacto, :fuerza, :defensa, :ataque, :herida, :valor, :p_poder, :p_voluntad, :p_destino, :descripcion
  json.url hero_url(hero, format: :json)
end
