json.array!(@equipos) do |equipo|
  json.extract! equipo, :id, :nombre, :descripcion, :puntos
  json.url equipo_url(equipo, format: :json)
end
