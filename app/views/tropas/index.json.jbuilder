json.array!(@tropas) do |tropa|
  json.extract! tropa, :id, :nombre, :puntos, :combate, :impacto, :fuerza, :defensa, :ataque, :herida, :valor, :descripcion
  json.url tropa_url(tropa, format: :json)
end
