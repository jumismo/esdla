class EquipoDisponible < ActiveRecord::Base
  belongs_to :equipo
  has_and_belongs_to_many :tropa
  has_and_belongs_to_many :hero
end
