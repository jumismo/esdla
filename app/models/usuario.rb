class Usuario < ActiveRecord::Base
  #attr_accessible :usuario, :password, :nombre, :password_confirmation
  attr_accessor :password
  before_save :encrypt_password
  has_many :lista_ejercito

  validates_confirmation_of :password
  #validates_presence_of :password, :on => :create
  #validates_presence_of :usuario, :on => :create
  #validates_presence_of :nombre, :on => :create
  validates_uniqueness_of :usuario

  def self.authenticate(usuario, password)
    user = find_by_usuario(usuario)
    if user && user.password_hash == BCrypt::Engine.hash_secret(password, user.password_salt)
      user
    else
      nil
    end
  end

  def encrypt_password
    if password.present?
      self.password_salt = BCrypt::Engine.generate_salt
      self.password_hash = BCrypt::Engine.hash_secret(password, password_salt)
    end
  end

  def initialize(attributes = {})
    super # must allow the active record to initialize!
    attributes.each do |name, value|
      send("#{name}=", value)
    end
  end

end
