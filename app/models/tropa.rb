class Tropa < ActiveRecord::Base
  belongs_to :ejercito
  has_and_belongs_to_many :reglas_especiale
  has_and_belongs_to_many :lista_ejercito
  has_and_belongs_to_many :equipo_disponible

  def ejercito
    ejercito = Ejercito.find(self.ejercito_id)
    ejercito.nombre
  end
end
