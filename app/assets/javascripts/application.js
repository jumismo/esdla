// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require turbolinks
//= require_tree .


// Observ field function

(function( $ ){

    jQuery.fn.observe_field = function(frequency, callback) {

        frequency = frequency * 100; // translate to milliseconds

        return this.each(function(){
            var $this = $(this);
            var prev = $this.val();

            var check = function() {
                if(removed()){ // if removed clear the interval and don't fire the callback
                    if(ti) clearInterval(ti);
                    return;
                }

                var val = $this.val();
                if(prev != val){
                    prev = val;
                    $this.map(callback); // invokes the callback on $this
                }
            };

            var removed = function() {
                return $this.closest('html').length == 0
            };

            var reset = function() {
                if(ti){
                    clearInterval(ti);
                    ti = setInterval(check, frequency);
                }
            };

            check();
            var ti = setInterval(check, frequency); // invoke check periodically

            // reset counter after user interaction
            $this.bind('keyup click mousemove', reset); //mousemove is for selects
        });

    };

})( jQuery );
