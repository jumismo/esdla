class TropasController < ApplicationController
  before_action :set_tropa, only: [:show, :edit, :update, :destroy]
  before_filter :authenticate_user
  before_filter :denied_access, :only => [:destroy]

  # GET /tropas
  # GET /tropas.json
  def index
    #@tropas = Tropa.all
    if params[:name].present? || params[:ejercito].present?
      scope = Tropa.all
      scope = scope.where("nombre like '%#{params[:name]}%'") if params[:name].present?
      scope = scope.where("ejercito_id = #{params[:ejercito]}") if params[:ejercito].present?
      scope = scope.order('ejercito_id asc').paginate(page: params[:page], per_page: 15)
      @tropas = scope
    else
      @tropas = Tropa.order('ejercito_id asc').paginate(page: params[:page], per_page: 15)
    end
  end

  # GET /tropas/1
  # GET /tropas/1.json
  def show
    @equipo_disponible = Equipo.find_by_sql("select e.nombre, e.descripcion, e.puntos
                                               from equipos e, equipo_disponibles ed, tropas_equipo_disponible ted, tropas t
                                              where e.id = ed.equipo_id
                                                and ed.tipo_miniatura = 'Tropa'
                                                and ed.id = ted.equipo_disponible_id
                                                and ted.tropa_id = t.id
                                                and t.id = #{@tropa.id}")
  end

  # GET /tropas/new
  def new
    @tropa = Tropa.new
  end

  # GET /tropas/1/edit
  def edit
  end

  # POST /tropas
  # POST /tropas.json
  def create
    @tropa = Tropa.new(tropa_params)

    respond_to do |format|
      if @tropa.save
        format.html { redirect_to @tropa, notice: "#{t 'tropas.index.mensaje_nuevo_ok' }" }
        format.json { render :show, status: :created, location: @tropa }
      else
        format.html { render :new }
        format.json { render json: @tropa.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /tropas/1
  # PATCH/PUT /tropas/1.json
  def update
    respond_to do |format|
      if @tropa.update(tropa_params)
        format.html { redirect_to @tropa, notice: "#{t 'tropas.index.mensaje_actualizo_ok' }" }
        format.json { render :show, status: :ok, location: @tropa }
      else
        format.html { render :edit }
        format.json { render json: @tropa.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /tropas/1
  # DELETE /tropas/1.json
  def destroy
    @tropa.destroy
    respond_to do |format|
      format.html { redirect_to tropas_url, notice: 'Tropa was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_tropa
      @tropa = Tropa.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def tropa_params
      params.require(:tropa).permit(:nombre, :puntos, :combate, :impacto, :fuerza, :defensa, :ataque, :herida, :valor, :descripcion, :ejercito_id)
    end
end
