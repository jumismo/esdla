class ReglasEspecialesController < ApplicationController
  before_action :set_reglas_especiale, only: [:show, :edit, :update, :destroy]
  before_filter :authenticate_user
  before_filter :denied_access, :only => [:destroy, :new, :create]

  # GET /reglas_especiales
  # GET /reglas_especiales.json
  def index
    @reglas_especiales = ReglasEspeciale.all
  end

  # GET /reglas_especiales/1
  # GET /reglas_especiales/1.json
  def show
  end

  # GET /reglas_especiales/new
  def new
    @reglas_especiale = ReglasEspeciale.new
  end

  # GET /reglas_especiales/1/edit
  def edit
  end

  # POST /reglas_especiales
  # POST /reglas_especiales.json
  def create
    @reglas_especiale = ReglasEspeciale.new(reglas_especiale_params)

    respond_to do |format|
      if @reglas_especiale.save
        format.html { redirect_to @reglas_especiale, notice: 'Reglas especiale was successfully created.' }
        format.json { render :show, status: :created, location: @reglas_especiale }
      else
        format.html { render :new }
        format.json { render json: @reglas_especiale.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /reglas_especiales/1
  # PATCH/PUT /reglas_especiales/1.json
  def update
    respond_to do |format|
      if @reglas_especiale.update(reglas_especiale_params)
        format.html { redirect_to @reglas_especiale, notice: 'Reglas especiale was successfully updated.' }
        format.json { render :show, status: :ok, location: @reglas_especiale }
      else
        format.html { render :edit }
        format.json { render json: @reglas_especiale.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /reglas_especiales/1
  # DELETE /reglas_especiales/1.json
  def destroy
    @reglas_especiale.destroy
    respond_to do |format|
      format.html { redirect_to reglas_especiales_url, notice: 'Reglas especiale was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_reglas_especiale
      @reglas_especiale = ReglasEspeciale.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def reglas_especiale_params
      params.require(:reglas_especiale).permit(:nombre, :descripcion)
    end
end
