class ListaEjercitosController < ApplicationController
  before_action :set_lista_ejercito, only: [:show, :edit, :update, :destroy]
  before_filter :authenticate_user

  # GET /lista_ejercitos
  # GET /lista_ejercitos.json
  def index
    scope = ListaEjercito.all
    scope = scope.where("usuario_id = #{current_user.id}").paginate(page: params[:page], per_page: 15)
    @lista_ejercitos = scope

  end

  # GET /lista_ejercitos/1
  # GET /lista_ejercitos/1.json
  def show
  end

  # GET /lista_ejercitos/new
  def new
    @lista_ejercito = ListaEjercito.new
  end

  # GET /lista_ejercitos/1/edit
  def edit
  end

  # POST /lista_ejercitos
  # POST /lista_ejercitos.json
  def create
    params[:lista_ejercito][:usuario_id] = current_user.id
    @lista_ejercito = ListaEjercito.new(lista_ejercito_params)

    respond_to do |format|
      if @lista_ejercito.save
        format.html { redirect_to @lista_ejercito, notice: 'Lista ejercito was successfully created.' }
        format.json { render :show, status: :created, location: @lista_ejercito }
      else
        format.html { render :new }
        format.json { render json: @lista_ejercito.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /lista_ejercitos/1
  # PATCH/PUT /lista_ejercitos/1.json
  def update
    respond_to do |format|
      if @lista_ejercito.update(lista_ejercito_params)
        format.html { redirect_to @lista_ejercito, notice: 'Lista ejercito was successfully updated.' }
        format.json { render :show, status: :ok, location: @lista_ejercito }
      else
        format.html { render :edit }
        format.json { render json: @lista_ejercito.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /lista_ejercitos/1
  # DELETE /lista_ejercitos/1.json
  def destroy
    @lista_ejercito.destroy
    respond_to do |format|
      format.html { redirect_to lista_ejercitos_url, notice: 'Lista ejercito was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def buscador
=begin
      Query para obtener los heroes y las tropas juntos

      select * from (select 1 as orden, nombre, puntos, combate, impacto, fuerza, defensa, ataque, herida, valor, null as p_poder, null as p_voluntad, null as p_destino, ejercito_id from tropas
      union
      select 2 as orden, nombre, puntos, combate, impacto, fuerza, defensa, ataque, herida, valor, p_poder, p_voluntad, p_destino, ejercito_id from heros) T order by ejercito_id asc, orden desc
=end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_lista_ejercito
      @lista_ejercito = ListaEjercito.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def lista_ejercito_params
      params.require(:lista_ejercito).permit(:nombre, :puntos_maximos, :puntos, :usuario_id)
    end
end
