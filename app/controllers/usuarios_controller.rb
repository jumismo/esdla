class UsuariosController < ApplicationController
  before_action :set_usuario, only: [:show, :edit, :update, :destroy]
  before_filter :authenticate_user, :except => [:logueo, :login]
  before_filter :denied_access, :only => [:index, :destroy, :new]

  # GET /usuarios
  # GET /usuarios.json
  def index
    @usuarios = Usuario.all
  end

  # GET /usuarios/1
  # GET /usuarios/1.json
  def show
  end

  # GET /usuarios/new
  def new
    @usuario = Usuario.new
  end

  # GET /usuarios/1/edit
  def edit
  end

  # POST /usuarios
  # POST /usuarios.json
  def create
    @usuario = Usuario.new(usuario_params)

    respond_to do |format|
      if @usuario.valid?
        if @usuario.save
          session[:usuario_id] = @usuario.id
          format.html { redirect_to @usuario, notice: 'Usuario was successfully created.' }
          format.json { render :show, status: :created, location: @usuario }
        else
          format.html { render :new }
          format.json { render json: @usuario.errors, status: :unprocessable_entity }
        end
      end
    end
  end

  # PATCH/PUT /usuarios/1
  # PATCH/PUT /usuarios/1.json
  def update
    respond_to do |format|
      if @usuario.update(usuario_params)
        format.html { redirect_to @usuario, notice: 'Usuario was successfully updated.' }
        format.json { render :show, status: :ok, location: @usuario }
      else
        format.html { render :edit }
        format.json { render json: @usuario.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /usuarios/1
  # DELETE /usuarios/1.json
  def destroy
    @usuario.destroy
    respond_to do |format|
      format.html { redirect_to usuarios_url, notice: 'Usuario was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def logueo
    @usuario = Usuario.new
  end

  def signed_out
    session[:usuario_id] = nil
    flash[:notice] = t 'common.m_deslogueo'
    redirect_to :root
  end

  def login
    usuario = params[:usuario]
    password = params[:password]
    usuario = Usuario.authenticate(usuario, password)
    if usuario
      session[:usuario_id] = usuario.id
      redirect_to :root
    else
      flash[:error] = t 'common.error_login'
      redirect_to :root
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_usuario
      @usuario = Usuario.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def usuario_params
      params.require(:usuario).permit(:usuario, :password, :nombre, :password_confirmation)
    end
end
