class EquipoDisponiblesController < ApplicationController
  before_action :set_equipo_disponible, only: [:show, :edit, :update, :destroy]
  before_filter :authenticate_user
  before_filter :denied_access, :only => [:destroy, :new, :create]

  # GET /equipo_disponibles
  # GET /equipo_disponibles.json
  def index
    @equipo_disponibles = EquipoDisponible.all
  end

  # GET /equipo_disponibles/1
  # GET /equipo_disponibles/1.json
  def show
  end

  # GET /equipo_disponibles/new
  def new
    @equipo_disponible = EquipoDisponible.new
  end

  # GET /equipo_disponibles/1/edit
  def edit
  end

  # POST /equipo_disponibles
  # POST /equipo_disponibles.json
  def create
    @equipo_disponible = EquipoDisponible.new(equipo_disponible_params)

    respond_to do |format|
      if @equipo_disponible.save
        format.html { redirect_to @equipo_disponible, notice: 'Equipo disponible was successfully created.' }
        format.json { render :show, status: :created, location: @equipo_disponible }
      else
        format.html { render :new }
        format.json { render json: @equipo_disponible.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /equipo_disponibles/1
  # PATCH/PUT /equipo_disponibles/1.json
  def update
    respond_to do |format|
      if @equipo_disponible.update(equipo_disponible_params)
        format.html { redirect_to @equipo_disponible, notice: 'Equipo disponible was successfully updated.' }
        format.json { render :show, status: :ok, location: @equipo_disponible }
      else
        format.html { render :edit }
        format.json { render json: @equipo_disponible.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /equipo_disponibles/1
  # DELETE /equipo_disponibles/1.json
  def destroy
    @equipo_disponible.destroy
    respond_to do |format|
      format.html { redirect_to equipo_disponibles_url, notice: 'Equipo disponible was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_equipo_disponible
      @equipo_disponible = EquipoDisponible.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def equipo_disponible_params
      params.require(:equipo_disponible).permit(:tipo_miniatura, :equipo_id)
    end
end
