class HerosController < ApplicationController
  before_action :set_hero, only: [:show, :edit, :update, :destroy]
  before_filter :authenticate_user
  before_filter :denied_access, :only => [:destroy]

  # GET /heros
  # GET /heros.json
  def index
#    @heros = Hero.all

    if params[:name].present? || params[:ejercito].present?
      scope = Hero.all
      scope = scope.where("nombre like '%#{params[:name]}%'") if params[:name].present?
      scope = scope.where("ejercito_id = #{params[:ejercito]}") if params[:ejercito].present?
      scope = scope.order('ejercito_id asc').paginate(page: params[:page], per_page: 15)
      @heros = scope
    else
      @heros = Hero.order('ejercito_id asc').paginate(page: params[:page], per_page: 15)
    end
  end

  # GET /heros/1
  # GET /heros/1.json
  def show
    @equipo_disponible = Equipo.find_by_sql("select e.nombre, e.descripcion, e.puntos from equipos e, equipo_disponibles ed, heroes_equipo_disponible hed, heros h
                                              where e.id = ed.equipo_id
                                                and ed.tipo_miniatura = 'Hero'
                                                and ed.id = hed.equipo_disponible_id
                                                and hed.heroe_id = h.id
                                                and h.id = #{@hero.id}")
  end

  # GET /heros/new
  def new
    @hero = Hero.new
  end

  # GET /heros/1/edit
  def edit
  end

  # POST /heros
  # POST /heros.json
  def create
    @hero = Hero.new(hero_params)

    respond_to do |format|
      if @hero.save
        format.html { redirect_to @hero, notice: "#{t 'heros.index.mensaje_nuevo_ok' }" }
        format.json { render :show, status: :created, location: @hero }
      else
        format.html { render :new }
        format.json { render json: @hero.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /heros/1
  # PATCH/PUT /heros/1.json
  def update
    respond_to do |format|
      if @hero.update(hero_params)
        format.html { redirect_to @hero, notice: "#{t 'heros.index.mensaje_actualizo_ok' }" }
        format.json { render :show, status: :ok, location: @hero }
      else
        format.html { render :edit }
        format.json { render json: @hero.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /heros/1
  # DELETE /heros/1.json
  def destroy
    @hero.destroy
    respond_to do |format|
      format.html { redirect_to heros_url, notice: 'Hero was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_hero
      @hero = Hero.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def hero_params
      params.require(:hero).permit(:nombre, :puntos, :combate, :impacto, :fuerza, :defensa, :ataque, :herida, :valor, :p_poder, :p_voluntad, :p_destino, :descripcion, :ejercito_id)
    end
end
