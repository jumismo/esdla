class EjercitosController < ApplicationController
  before_action :set_ejercito, only: [:show, :edit, :update, :destroy]
  before_filter :authenticate_user
  before_filter :denied_access, :only => [:destroy, :new, :create]

  # GET /ejercitos
  # GET /ejercitos.json
  def index
    @ejercitos = Ejercito.all
  end

  # GET /ejercitos/1
  # GET /ejercitos/1.json
  def show
  end

  # GET /ejercitos/new
  def new
    @ejercito = Ejercito.new
  end

  # GET /ejercitos/1/edit
  def edit
  end

  # POST /ejercitos
  # POST /ejercitos.json
  def create
    @ejercito = Ejercito.new(ejercito_params)

    respond_to do |format|
      if @ejercito.save
        format.html { redirect_to @ejercito, notice: "#{t 'ejercitos.index.mensaje_nuevo_ok' }"}
        format.json { render :show, status: :created, location: @ejercito }
      else
        format.html { render :new }
        format.json { render json: @ejercito.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /ejercitos/1
  # PATCH/PUT /ejercitos/1.json
  def update
    respond_to do |format|
      if @ejercito.update(ejercito_params)
        format.html { redirect_to @ejercito, notice: "#{t 'ejercitos.index.mensaje_actualizo_ok' }" }
        format.json { render :show, status: :ok, location: @ejercito }
      else
        format.html { render :edit }
        format.json { render json: @ejercito.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /ejercitos/1
  # DELETE /ejercitos/1.json
  def destroy
    @ejercito.destroy
    respond_to do |format|
      format.html { redirect_to ejercitos_url, notice: 'Ejercito was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ejercito
      @ejercito = Ejercito.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def ejercito_params
      params.require(:ejercito).permit(:nombre, :descripcion)
    end
end
